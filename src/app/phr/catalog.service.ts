import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';
import {Observable} from 'rxjs/Rx';

import {Plant, Query} from './declarations';

@Injectable()
export class PlantCatalogService {
    plants: Plant[] = [];
    constructor(public http: Http) {}
    
    executeQuery(query: Query) {
        this.http.get('http://192.168.99.100:3000/api/inventory/plants')
            .subscribe(response => this.plants = response.json());
    }
}
